# Quality tools for a basic dotnet project

## Demo application

## Sonar

Run the Sonar server with Docker

```powershell
docker compose up -d
```

log to the server on http://localhost:9000 with admin / admin as credentials.
Change your admin credentials for `sonarpwd1234`

### Dotnet Core scanner

Install the sonarscanner for dotnet core projects

```powershell
dotnet tool install --global dotnet-sonarscanner
```

Run the Analysis for dotnet Core

```powershell
cd app/
dotnet sonarscanner begin /k:"app" /d:sonar.login="admin" /d:sonar.password="sonarpwd1234"
dotnet build --no-incremental
dotnet sonarscanner end /d:sonar.login="admin" /d:sonar.password="sonarpwd1234"
```

### Dotnet Framework scanner

Install the sonarscanner for framework projects

```powerhsell
Expand-Archive -Path .\bin\sonar-scanner-6.2.0.85879-net-framework.zip -DestinationPath .\bin\scanner
$env:PATH += ";$PWD/bin/scanner"
```

Run the Analysis for framework projects

```powershell
cd app/
SonarScanner.MSBuild.exe begin /k:"key" /d:sonar.login="admin" /d:sonar.password="sonarpwd1234"
MSBuild.exe /t:Rebuild
SonarScanner.MSBuild.exe end /d:sonar.login="admin" /d:sonar.password="sonarpwd1234"
```


## Vulnerabilities

Just a little test with dotnet CLI

```
dotnet list package --vulnerable
```

Install 

```
Expand-Archive -Path .\bin\dependency-check.zip -DestinationPath .\bin\
```


