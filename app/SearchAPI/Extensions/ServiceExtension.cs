﻿using SearchProvider;
using Entities.Settings;
using Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Configuration;

namespace SearchAPI.Extensions
{
    public static class ServiceExtension
    {
        /// <summary>
        /// Add SearchProvider service to the scope of the API
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureSearchProviderWrapper(this IServiceCollection services)
        {
            services.AddScoped<ISearchProviderWrapper, SearchProviderWrapper>();
        }

        /// <summary>
        /// Add ES Cloud settings to the configuration of the services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <exception cref="Exception"></exception>
        public static void ConfigureSettings(this IServiceCollection services, IConfiguration configuration)
        {
            var _esCloudSettingsConfigurationSection = configuration.GetSection(nameof(ESCloudSettings));
            if (_esCloudSettingsConfigurationSection == null)
                throw new Exception("No appsettings has been found");

            services.Configure<ESCloudSettings>(_esCloudSettingsConfigurationSection);
        }

        /// <summary>
        /// Add Application Insights settings
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <exception cref="Exception"></exception>
        public static void ConfigureAppInsightSettings(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationInsightsConnectionString = configuration["ApplicationInsights:ConnectionString"];
            if (applicationInsightsConnectionString == null)
                throw new Exception("No appsettings has been found");

            services.AddApplicationInsightsTelemetry(applicationInsightsConnectionString);
        }

        /// <summary>
        /// Configure JWT authentication
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureAuthenticationAndAuthorization(this IServiceCollection services)
        {
            services.AddAuthentication().AddJwtBearer();
            services.AddAuthorization();
        }

        /// <summary>
        /// Configure Swagger for development environment
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddEndpointsApiExplorer();
            //builder.Services.AddSwaggerGen();

            //Use Swagger with Authorization header
            services.AddSwaggerGen(c =>
            {
                var securitySchema = new OpenApiSecurityScheme
                {
                    Description = "Using the Authorization header with the Bearer scheme.",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                };

                c.AddSecurityDefinition("Bearer", securitySchema);

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
          {
              { securitySchema, new[] { "Bearer" } }
          });
            });
        }
    }
}
