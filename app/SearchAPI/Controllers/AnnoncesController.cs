﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Entities.Models;
using Contracts;
using Elastic.Clients.Elasticsearch;
using Microsoft.AspNetCore.Authorization;

namespace SearchAPI.Controllers
{
    [ApiController]
    [Route("searchapi/[controller]/[action]")]
    public class AnnoncesController : ControllerBase
    {
        private ISearchProviderWrapper _searchProvider;
        private readonly ILogger<AnnoncesController> _logger;

        public AnnoncesController(ILogger<AnnoncesController> logger, ISearchProviderWrapper searchProvider)
        {
            _logger = logger;
            _searchProvider = searchProvider;
        }

        #region GET
        // GET: /Annonces/Test
        /// <summary>
        /// Test to check that endpoint is correct
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Annonce))]
        public string Test()
        {
            return "OK";
        }

        // GET: /Annonces/SearchText/{text to search}
        /// <summary>
        /// Search for a text
        /// </summary>
        /// <param name="textToSearch"></param>
        /// <param name="hitsToSkip"></param>
        /// <param name="size"></param>
        /// <returns>List of paginated items that contains the "text to search"</returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Annonce))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(Annonce))]
        [Authorize]
        public async Task<IEnumerable<Annonce>> SearchText(string textToSearch, int? hitsToSkip, int? size)
       {
            _logger.LogDebug($"AnnoncesControllers::SearchText::{textToSearch}");



            if (string.IsNullOrEmpty(textToSearch))
                throw new ArgumentNullException("SearchText:textToSearch");

            var g1 = new Guid();

            var response = await _searchProvider.AnnonceSearchProvider.SearchTextPaginatedAsync(textToSearch, hitsToSkip, size);
            return response;
       }

        // GET: /Annonces/SearchAll
        /// <summary>
        /// Search all items
        /// </summary>
        /// <returns>Returns all items</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Annonce))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(Annonce))]
        [Authorize]
        public async Task<IEnumerable<Annonce>> SearchAll()
        {
            _logger.LogDebug($"AnnoncesControllers::SearchAll");

            var response = await _searchProvider.AnnonceSearchProvider.SearchAllAnnoncesAsync();
            return response;
        }

        // GET: /Annonces/Autocomplete
        /// <summary>
        /// Autocomplete for input text
        /// </summary>
        /// <param name="inputText"></param>
        /// <returns>List of items that starts with the input text</returns>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Annonce))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(Annonce))]
        [Authorize]
        public async Task<IEnumerable<Annonce>> Autocomplete(string inputText)
        {
            _logger.LogDebug($"AnnoncesControllers::Autocomplete");

            if (string.IsNullOrEmpty(inputText))
                throw new ArgumentNullException("Autocomplete:inputText");

            var response = await _searchProvider.AnnonceSearchProvider.AutocompleteAsync(inputText);
            return response;
        }
        #endregion

    }
}
