using Microsoft.OpenApi.Models;
using SearchAPI.Extensions;
using SearchAPI.Middlewares;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// add searchProviderWrapper
builder.Services.ConfigureSearchProviderWrapper();
// add settings in services
builder.Services.ConfigureSettings(builder.Configuration);
builder.Services.ConfigureAppInsightSettings(builder.Configuration);
// add authentication and authorization conf and middlewares
builder.Services.ConfigureAuthenticationAndAuthorization();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.ConfigureSwagger();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseDeveloperExceptionPage();
    app.UseHsts();
}
else
{
    // Configure Exceptions handling
    app.UseMiddleware(typeof(ExceptionMiddleware));
}

//app.UseAuthorization();

app.UseHttpsRedirection();

app.MapControllers();

app.Run();
