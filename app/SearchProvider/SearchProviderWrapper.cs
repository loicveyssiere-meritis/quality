﻿using Contracts;
using Elastic.Clients.Elasticsearch;
using Elastic.Transport;
using Entities.Settings;
using Microsoft.Extensions.Options;

namespace SearchProvider
{
    public class SearchProviderWrapper : ISearchProviderWrapper
    {
        private readonly ESCloudSettings _esSettings;

        private ElasticsearchClient _elasticClient;
        private IAnnonceProvider _annonceProvider;

        public SearchProviderWrapper(IOptions<ESCloudSettings> esCloudSettings)
        {
            _esSettings = esCloudSettings.Value;
        }
        public IAnnonceProvider AnnonceSearchProvider {
            get {
                if (_annonceProvider == null )
                {
                    _annonceProvider = new AnnonceProvider(ElasticsClient);
                }

                return _annonceProvider; 
            }
            
        }

        private ElasticsearchClient ElasticsClient { 
            get
            {
                if (_elasticClient == null)
                {
                    if (_esSettings != null)
                    {
                        var cloudID = _esSettings.CloudID;
                        var apiKey = _esSettings.ApiKey;
                        var defaultIndexName = _esSettings.DefaultIndexName;

                        var settings = new ElasticsearchClientSettings(cloudID, new ApiKey(apiKey))
                        .DefaultFieldNameInferrer(p => p)
                        .DefaultIndex(defaultIndexName);

                        _elasticClient = new ElasticsearchClient(settings);
                    }
                    else
                        throw new Exception("SearchProviderWrapper : ES Cloud Settings not found");
                    
                }
                return _elasticClient;
            } 
        }
    }
}
