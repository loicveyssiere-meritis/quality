﻿using Contracts;
using Elastic.Clients.Elasticsearch;

namespace SearchProvider
{
    public class SearchProviderBase<T> where T : class
    {
        protected ElasticsearchClient ESClient;
        public SearchProviderBase(ElasticsearchClient eSClient)
        {
            ESClient = eSClient;
        }
    }
}
