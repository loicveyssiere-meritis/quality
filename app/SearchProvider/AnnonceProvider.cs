﻿using Contracts;
using Entities.Models;
using Elastic.Clients.Elasticsearch;
using Elastic.Clients.Elasticsearch.QueryDsl;
using System.Configuration;

namespace SearchProvider
{
    public class AnnonceProvider : SearchProviderBase<Annonce>, IAnnonceProvider
    {
        public AnnonceProvider(ElasticsearchClient client): base(client) { 
            
        }
        /// <summary>
        /// Search all annonces in ES index
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<IReadOnlyCollection<Annonce>> SearchAllAnnoncesAsync()
        {
            var searchResponse = await ESClient.SearchAsync<Annonce>(s => s
                .Query(q => q
                    .MatchAll()
                       )
            );

            if (searchResponse.IsValidResponse)
            {
                return searchResponse.Documents;
            }
            else
            {
                var debug = searchResponse.DebugInformation;
                throw new NotImplementedException(debug);
            }
        }
        /// <summary>
        /// Search for a text in ES index
        /// </summary>
        /// <param name="inputText"></param>
        /// <returns>All items that contains the text to search</returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<IReadOnlyCollection<Annonce>> SearchTextAsync(string inputText)
        {
            var searchResponse = await ESClient.SearchAsync<Annonce>(s => s
                .Query(q => q
                    .MultiMatch(m => m
                        .Fields(new[] { "Titre", "Titre.folded", "Description" })
                        .Query(inputText)
                        .Fuzziness(new Fuzziness("AUTO"))
                        )
                )
            );

            if (searchResponse.IsValidResponse)
            {
                return searchResponse.Documents;
            }
            else
            {
                var debug = searchResponse.DebugInformation;
                throw new NotImplementedException(debug);
            }
        }
        /// <summary>
        /// Search for a text in ES index. Result is paginated
        /// </summary>
        /// <param name="inputText"></param>
        /// <param name="numberHitsToSkip"></param>
        /// <param name="size"></param>
        /// <returns>Paginated list with items that contains the text to search</returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<IReadOnlyCollection<Annonce>> SearchTextPaginatedAsync(string inputText, int? numberHitsToSkip, int? size)
        {
            var searchResponse = await ESClient.SearchAsync<Annonce>(s => s
                .Query(q => q
                    .MultiMatch(m => m
                        .Fields(new[] { "Titre", "Titre.folded", "Description" })
                        .Query(inputText)
                        .Fuzziness(new Fuzziness("AUTO"))
                        )
                )
                .From(numberHitsToSkip)
                .Size(size)
            );

            if (searchResponse.IsValidResponse)
            {
                return searchResponse.Documents;
            }
            else
            {
                var debug = searchResponse.DebugInformation;
                throw new NotImplementedException(debug);
            }
        }
        /// <summary>
        /// list of documents in ES index that starts with the input text
        /// </summary>
        /// <param name="inputText"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<IReadOnlyCollection<Annonce>> AutocompleteAsync(string inputText)
        {
            var searchResponse = await ESClient.SearchAsync<Annonce>(s => s
                .Query(q => q
                    .MultiMatch(m => m
                        .Type(TextQueryType.BoolPrefix)
                        .Fields(new[] { "Titre.suggest", "Titre.folded", "Titre.suggest._2gram", "Titre.suggest._3gram" })
                        .Query(inputText)
                        .Fuzziness(new Fuzziness("AUTO"))
                        )
                )
           );

            if (searchResponse.IsValidResponse)
            {
                return searchResponse.Documents;
            }
            else
            {
                var debug = searchResponse.DebugInformation;
                throw new NotImplementedException(debug);
            }
        }
    }
}
