﻿
namespace Contracts
{
    public interface ISearchProviderWrapper
    {
        IAnnonceProvider AnnonceSearchProvider { get; }
    }
}
