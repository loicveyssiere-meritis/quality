﻿namespace Contracts
{
    public interface ISearchProviderBase<T>
    {
        Task<IReadOnlyCollection<T>> SearchTextAsync(string inputText);
        Task<IReadOnlyCollection<T>> SearchTextPaginatedAsync(string inputText, int? numberHitsToSkip, int? size);
        Task<IReadOnlyCollection<T>> AutocompleteAsync(string inputText);
    }
}
