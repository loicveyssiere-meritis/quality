﻿using Entities.Models;

namespace Contracts
{
    public interface IAnnonceProvider : ISearchProviderBase<Annonce>
    {
        public Task<IReadOnlyCollection<Annonce>> SearchAllAnnoncesAsync();
    }
}
