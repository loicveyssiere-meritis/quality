﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Settings
{
    public class ESCloudSettings
    {
        public string CloudID { get; set; }
        public string ApiKey { get; set; }
        public string DefaultIndexName { get; set; }
    }
}
